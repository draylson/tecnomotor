package model;

import java.util.ArrayList;
import java.util.List;

import dao.AplicacaoDAO;
import dao.ConectorDAO;
import dao.MontadoraDAO;
import dao.SistemaDAO;
import dao.TipoSistemaDAO;
import dao.VeiculoDAO;
import entity.Aplicacao;
import entity.Conector;
import entity.Montadora;
import entity.Sistema;
import entity.TipoSistema;
import entity.Veiculo;

public class AplicacaoViewModel {

	Aplicacao aplicacao = null;

	List<Conector> conectores = new ArrayList<Conector>();

	List<Veiculo> veiculos = new ArrayList<Veiculo>();

	List<Montadora> montadoras = new ArrayList<Montadora>();

	List<Sistema> sistemas = new ArrayList<Sistema>();

	List<TipoSistema> tiposDeSistemas = new ArrayList<TipoSistema>();
	
	List<AplicacaoObserver> observers = new ArrayList<AplicacaoObserver>();

	public AplicacaoViewModel(Aplicacao aplicacao) {
		this.aplicacao = aplicacao;
		init();
	}

	public AplicacaoViewModel() {
		this.aplicacao = new Aplicacao();
		init();
	}

	public int getAnoInicial() {
		return aplicacao.getAnoInicial();
	}

	public void setAnoInicial(int anoInicial) {
		this.aplicacao.setAnoInicial(anoInicial);
	}

	public int getAnoFinal() {
		return this.aplicacao.getAnoFinal();
	}

	public void setAnoFinal(int anoFinal) {
		this.aplicacao.setAnoFinal(anoFinal);
	}

	public TipoSistema getTipoSistema() {
		return this.aplicacao.getTipoSistema();
	}

	public void setTipoSistema(TipoSistema tipoSistema) {
		this.aplicacao.setTipoSistema(tipoSistema);
	}

	public Sistema getSistema() {
		return this.aplicacao.getSistema();
	}

	public void setSistema(Sistema sistema) {
		this.aplicacao.setSistema(sistema);
	}

	public Montadora getMontadora() {
		return this.aplicacao.getMontadora();
	}

	public void setMontadora(Montadora montadora) {
		this.aplicacao.setMontadora(montadora);
	}

	public Veiculo getVeiculo() {
		return this.aplicacao.getVeiculo();
	}

	public void setVeiculo(Veiculo veiculo) {
		this.aplicacao.setVeiculo(veiculo);
	}
	
	public List<Conector> getAplicacaoConectores() {
		return this.aplicacao.getConectores();
	}

	public List<Conector> getConectores() {
		return new ArrayList<Conector>(conectores);
	}

	public List<Veiculo> getVeiculos() {
		return new ArrayList<Veiculo>(veiculos);
	}

	public List<Montadora> getMontadoras() {
		return new ArrayList<Montadora>(montadoras);
	}

	public List<Sistema> getSistemas() {
		return new ArrayList<Sistema>(sistemas);
	}

	public List<TipoSistema> getTiposDeSistemas() {
		return new ArrayList<TipoSistema>(tiposDeSistemas);
	}
	
	public void addConector(Conector conector) {
		aplicacao.getConectores().add(conector);
		update();
	}
	
	public void salvar() {
		
		AplicacaoDAO dao = new AplicacaoDAO();
		
		if(aplicacao.getId() == null) {
			dao.create(aplicacao);
		} else {
			dao.update(aplicacao);
		}
		
	}
	
	public void deletar() {
		AplicacaoDAO dao = new AplicacaoDAO();
		dao.delete(aplicacao);
	}
	
	public void addObserver(AplicacaoObserver observer) {
		observers.add(observer);
	}
	
	public void removeObserver(AplicacaoObserver observer) {
		observers.remove(observer);
	}

	private void init() {
		
		conectores.addAll(new ConectorDAO().readAll());
		veiculos.addAll(new VeiculoDAO().readAll());
		montadoras.addAll(new MontadoraDAO().readAll());
		sistemas.addAll(new SistemaDAO().readAll());
		tiposDeSistemas.addAll(new TipoSistemaDAO().readAll());
		
		update();
		
	}

	private void update() {
		for(AplicacaoObserver observer : observers)
			observer.updateAplicacaoView();
	}

}
