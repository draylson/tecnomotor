package model;

public interface MainObserver {
	
	public void update();

}
