package model;

import java.util.ArrayList;
import java.util.List;

import dao.AplicacaoDAO;
import dao.ConectorDAO;
import dao.MontadoraDAO;
import dao.SistemaDAO;
import dao.TipoSistemaDAO;
import dao.VeiculoDAO;
import entity.Aplicacao;
import entity.Conector;
import entity.Montadora;
import entity.Sistema;
import entity.TipoSistema;
import entity.Veiculo;

public class MainViewModel {
	
	List<Aplicacao> aplicacoes = new ArrayList<Aplicacao>();
	
	List<Conector> conectores = new ArrayList<Conector>();
	
	List<Veiculo> veiculos = new ArrayList<Veiculo>();
	
	List<Montadora> montadoras = new ArrayList<Montadora>();
	
	List<Sistema> sistemas = new ArrayList<Sistema>();
	
	List<TipoSistema> tiposDeSistemas = new ArrayList<TipoSistema>();
	
	List<MainObserver> observers = new ArrayList<MainObserver>();

	public MainViewModel() {
		update();
	}

	public List<Aplicacao> getAplicacoes() {
		return new ArrayList<Aplicacao>(aplicacoes);
	}

	public List<Conector> getConectores() {
		return new ArrayList<Conector>(conectores);
	}

	public List<Veiculo> getVeiculos() {
		return new ArrayList<Veiculo>(veiculos);
	}

	public List<Montadora> getMontadoras() {
		return new ArrayList<Montadora>(montadoras);
	}

	public List<Sistema> getSistemas() {
		return new ArrayList<Sistema>(sistemas);
	}

	public List<TipoSistema> getTiposDeSistemas() {
		return new ArrayList<TipoSistema>(tiposDeSistemas);
	}
	
	public void addObserver(MainObserver observer) {
		observers.add(observer);
		update();
	}
	
	public void removeObserver(MainObserver observer) {
		observers.remove(observer);
		update();
	}
	
	public void addConector(String nome) {
		Conector conector = new Conector();
		conector.setNome(nome);
		ConectorDAO dao = new ConectorDAO();
		dao.create(conector);
		update();
	}
	
	public void addVeiculo(String nome) {
		Veiculo veiculo = new Veiculo();
		veiculo.setNome(nome);
		VeiculoDAO dao = new VeiculoDAO();
		dao.create(veiculo);
		update();
	}
	
	public void addMontadora(String nome) {
		Montadora montadora = new Montadora();
		montadora.setNome(nome);
		MontadoraDAO dao = new MontadoraDAO();
		dao.create(montadora);
		update();
	}
	
	public void addSistema(String nome) {
		Sistema sistema = new Sistema();
		sistema.setNome(nome);
		SistemaDAO dao = new SistemaDAO();
		dao.create(sistema);
		update();
	}
	
	public void addTipoSistema(String nome) {
		TipoSistema tipoSistema = new TipoSistema();
		tipoSistema.setNome(nome);
		TipoSistemaDAO dao = new TipoSistemaDAO();
		dao.create(tipoSistema);
		update();
	}
	
	public void removeConector(Conector conector) {
		ConectorDAO dao = new ConectorDAO();
		dao.delete(conector);
		update();
	}
	
	public void removeVeiculo(Veiculo veiculo) {
		VeiculoDAO dao = new VeiculoDAO();
		dao.delete(veiculo);
		update();
	}
	
	public void removeMontadora(Montadora montadora) {
		MontadoraDAO dao = new MontadoraDAO();
		dao.delete(montadora);
		update();
	}
	
	public void removeSistema(Sistema sistema) {
		SistemaDAO dao = new SistemaDAO();
		dao.delete(sistema);
		update();
	}
	
	public void removeTipoSistema(TipoSistema tipoSistema) {
		TipoSistemaDAO dao = new TipoSistemaDAO();
		dao.delete(tipoSistema);
		update();
	}

	private void update() {
		aplicacoes.clear();
		conectores.clear();
		veiculos.clear();
		montadoras.clear();
		sistemas.clear();
		tiposDeSistemas.clear();
		aplicacoes.addAll(new AplicacaoDAO().readAll());
		conectores.addAll(new ConectorDAO().readAll());
		veiculos.addAll(new VeiculoDAO().readAll());
		montadoras.addAll(new MontadoraDAO().readAll());
		sistemas.addAll(new SistemaDAO().readAll());
		tiposDeSistemas.addAll(new TipoSistemaDAO().readAll());
		updateObservers();
	}

	private void updateObservers() {
		for(MainObserver observer : observers)
			observer.update();
	}

}
