package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import entity.Aplicacao;
import entity.Conector;
import entity.Montadora;
import entity.Sistema;
import entity.TipoSistema;
import entity.Veiculo;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import main.Tecnomotor;
import model.MainObserver;
import model.MainViewModel;

public class MainViewController implements Initializable, MainObserver {
	
	private static Stage aplicacaoStage = null;

	public static Stage getAplicacaoStage() {
		return aplicacaoStage;
	}

	MainViewModel mainViewModel = new MainViewModel();

	@FXML
	TableView<Aplicacao> tvAplicacoes;

	@FXML
	TableColumn<Aplicacao, String> tcAplicacaoTipoSistema;

	@FXML
	TableColumn<Aplicacao, String> tcAplicacaoMontadora;

	@FXML
	TableColumn<Aplicacao, String> tcAplicacaoVeiculo;

	@FXML
	TableColumn<Aplicacao, String> tcAplicacaoSistema;

	@FXML
	TableColumn<Aplicacao, String> tcAplicacaoAnoInicial;

	@FXML
	TableColumn<Aplicacao, String> tcAplicacaoAnoFinal;

	@FXML
	TableColumn<Aplicacao, String> tcAplicacaoConectores;

	@FXML
	TableView<Conector> tvConectores;

	@FXML
	TableColumn<Conector, String> tcConectores;

	@FXML
	TableView<Veiculo> tvVeiculos;

	@FXML
	TableColumn<Veiculo, String> tcVeiculos;

	@FXML
	TableView<Montadora> tvMontadoras;

	@FXML
	TableColumn<Montadora, String> tcMontadoras;

	@FXML
	TableView<Sistema> tvSistemas;

	@FXML
	TableColumn<Sistema, String> tcSistemas;

	@FXML
	TableView<TipoSistema> tvTiposDeSistemas;

	@FXML
	TableColumn<TipoSistema, String> tcTiposDeSistemas;

	@FXML
	TextField tfConector;

	@FXML
	TextField tfVeiculo;

	@FXML
	TextField tfMontadora;

	@FXML
	TextField tfSistema;

	@FXML
	TextField tfTipoSistema;

	public void adicionarConector() {
		mainViewModel.addConector(tfConector.getText());
		tfConector.setText("");
	}

	public void adicionarVeiculo() {
		mainViewModel.addVeiculo(tfVeiculo.getText());
		tfVeiculo.setText("");
	}

	public void adicionarMontadora() {
		mainViewModel.addMontadora(tfMontadora.getText());
		tfMontadora.setText("");
	}

	public void adicionarSistema() {
		mainViewModel.addSistema(tfSistema.getText());
		tfSistema.setText("");
	}

	public void adicionarTipoSistema() {
		mainViewModel.addTipoSistema(tfTipoSistema.getText());
		tfTipoSistema.setText("");
	}

	public void removerConector() {
		mainViewModel.removeConector(tvConectores.getSelectionModel().getSelectedItem());
	}

	public void removerVeiculo() {
		mainViewModel.removeVeiculo(tvVeiculos.getSelectionModel().getSelectedItem());
	}

	public void removerMontadora() {
		mainViewModel.removeMontadora(tvMontadoras.getSelectionModel().getSelectedItem());
	}

	public void removerSistema() {
		mainViewModel.removeSistema(tvSistemas.getSelectionModel().getSelectedItem());
	}

	public void removerTipoSistema() {
		mainViewModel.removeTipoSistema(tvTiposDeSistemas.getSelectionModel().getSelectedItem());
	}

	public void callAplicacaoStage() {
		
		try {
		
			Parent root = FXMLLoader.load(this.getClass().getResource("/view/AplicacaoView.fxml"));
			Scene scene = new Scene(root);

			aplicacaoStage = new Stage();
			aplicacaoStage.initModality(Modality.WINDOW_MODAL);
			aplicacaoStage.initOwner(Tecnomotor.getPrimaryStage());
			aplicacaoStage.setScene(scene);
			aplicacaoStage.setTitle("Aplicação");
			aplicacaoStage.showAndWait();
			
			mainViewModel = new MainViewModel();
			update();
		
		} catch (IOException e) {
		
			e.printStackTrace();
		
		}
		
	}

	@Override
	public void update() {

		tvAplicacoes.setItems(FXCollections.observableArrayList(mainViewModel.getAplicacoes()));
		tvConectores.setItems(FXCollections.observableArrayList(mainViewModel.getConectores()));
		tvVeiculos.setItems(FXCollections.observableArrayList(mainViewModel.getVeiculos()));
		tvMontadoras.setItems(FXCollections.observableArrayList(mainViewModel.getMontadoras()));
		tvSistemas.setItems(FXCollections.observableArrayList(mainViewModel.getSistemas()));
		tvTiposDeSistemas.setItems(FXCollections.observableArrayList(mainViewModel.getTiposDeSistemas()));

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		tcAplicacaoTipoSistema
				.setCellValueFactory(new Callback<CellDataFeatures<Aplicacao, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(CellDataFeatures<Aplicacao, String> c) {
						return new SimpleStringProperty(c.getValue().getTipoSistema().getNome());
					}
				});

		tcAplicacaoMontadora
				.setCellValueFactory(new Callback<CellDataFeatures<Aplicacao, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(CellDataFeatures<Aplicacao, String> c) {
						return new SimpleStringProperty(c.getValue().getMontadora().getNome());
					}
				});

		tcAplicacaoVeiculo
				.setCellValueFactory(new Callback<CellDataFeatures<Aplicacao, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(CellDataFeatures<Aplicacao, String> c) {
						return new SimpleStringProperty(c.getValue().getVeiculo().getNome());
					}
				});

		tcAplicacaoSistema
				.setCellValueFactory(new Callback<CellDataFeatures<Aplicacao, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(CellDataFeatures<Aplicacao, String> c) {
						return new SimpleStringProperty(c.getValue().getSistema().getNome());
					}
				});

		tcAplicacaoAnoInicial
				.setCellValueFactory(new Callback<CellDataFeatures<Aplicacao, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(CellDataFeatures<Aplicacao, String> c) {
						return new SimpleStringProperty(String.valueOf(c.getValue().getAnoInicial()));
					}
				});

		tcAplicacaoAnoFinal
				.setCellValueFactory(new Callback<CellDataFeatures<Aplicacao, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(CellDataFeatures<Aplicacao, String> c) {
						return new SimpleStringProperty(String.valueOf(c.getValue().getAnoFinal()));
					}
				});

		tcAplicacaoConectores
				.setCellValueFactory(new Callback<CellDataFeatures<Aplicacao, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(CellDataFeatures<Aplicacao, String> c) {

						String conectores = "";

						for (Conector conector : c.getValue().getConectores())
							conectores += conector.getNome() + ", ";

						conectores = conectores.substring(0, conectores.lastIndexOf(","));

						return new SimpleStringProperty(conectores);

					}
				});

		tcConectores.setCellValueFactory(new Callback<CellDataFeatures<Conector, String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<Conector, String> c) {
				return new SimpleStringProperty(c.getValue().getNome());
			}
		});

		tcVeiculos.setCellValueFactory(new Callback<CellDataFeatures<Veiculo, String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<Veiculo, String> c) {
				return new SimpleStringProperty(c.getValue().getNome());
			}
		});

		tcMontadoras.setCellValueFactory(new Callback<CellDataFeatures<Montadora, String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<Montadora, String> c) {
				return new SimpleStringProperty(c.getValue().getNome());
			}
		});

		tcSistemas.setCellValueFactory(new Callback<CellDataFeatures<Sistema, String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<Sistema, String> c) {
				return new SimpleStringProperty(c.getValue().getNome());
			}
		});

		tcTiposDeSistemas
				.setCellValueFactory(new Callback<CellDataFeatures<TipoSistema, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(CellDataFeatures<TipoSistema, String> c) {
						return new SimpleStringProperty(c.getValue().getNome());
					}
				});

		mainViewModel.addObserver(this);

	}

}
