package controller;

import java.net.URL;
import java.util.ResourceBundle;

import entity.Conector;
import entity.Montadora;
import entity.Sistema;
import entity.TipoSistema;
import entity.Veiculo;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.util.Callback;
import model.AplicacaoObserver;
import model.AplicacaoViewModel;

public class AplicacaoViewController implements Initializable, AplicacaoObserver {

	AplicacaoViewModel aplicacaoViewModel = new AplicacaoViewModel();
	
	@FXML
	ChoiceBox<TipoSistema> cbTiposDeSistemas;
	
	@FXML
	ChoiceBox<Montadora> cbMontadoras;
	
	@FXML
	ChoiceBox<Veiculo> cbVeiculos;
	
	@FXML
	ChoiceBox<Sistema> cbSistemas;
	
	@FXML
	ChoiceBox<Conector> cbConectores;
	
	@FXML
	TextField tfAnoInicial;
	
	@FXML
	TextField tfAnoFinal;
	
	@FXML
	TableView<Conector> tvConectores;
	
	@FXML
	TableColumn<Conector, String> tcConectores;
	
	public void changeTipoSistema() {
		aplicacaoViewModel.setTipoSistema(cbTiposDeSistemas.getValue());
	}
	
	public void changeMontadora() {
		aplicacaoViewModel.setMontadora(cbMontadoras.getValue());
	}
	
	public void changeVeiculo() {
		aplicacaoViewModel.setVeiculo(cbVeiculos.getValue());
	}
	
	public void changeSistema() {
		aplicacaoViewModel.setSistema(cbSistemas.getValue());
	}
	
	public void changeAnoInicial() {
		aplicacaoViewModel.setAnoInicial(Integer.parseInt(tfAnoInicial.getText()));
	}
	
	public void changeAnoFinal() {
		aplicacaoViewModel.setAnoFinal(Integer.parseInt(tfAnoFinal.getText()));
	}
	
	public void adicionarConector() {
		aplicacaoViewModel.addConector(cbConectores.getValue());
	}
	
	public void salvar() {
		aplicacaoViewModel.salvar();
		MainViewController.getAplicacaoStage().close();
	}
	
	public void deletar() {
		aplicacaoViewModel.deletar();
		MainViewController.getAplicacaoStage().close();
	}
	
	public void cancelar() {
		MainViewController.getAplicacaoStage().close();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		cbTiposDeSistemas.getItems().addAll(aplicacaoViewModel.getTiposDeSistemas());
		cbMontadoras.getItems().addAll(aplicacaoViewModel.getMontadoras());
		cbVeiculos.getItems().addAll(aplicacaoViewModel.getVeiculos());
		cbSistemas.getItems().addAll(aplicacaoViewModel.getSistemas());
		cbConectores.getItems().addAll(aplicacaoViewModel.getConectores());

		tcConectores.setCellValueFactory(new Callback<CellDataFeatures<Conector, String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<Conector, String> c) {
				return new SimpleStringProperty(c.getValue().getNome());
			}
		});
		
		aplicacaoViewModel.addObserver(this);
		
	}

	@Override
	public void updateAplicacaoView() {
		tvConectores.setItems(FXCollections.observableArrayList(aplicacaoViewModel.getAplicacaoConectores()));
	}

}
