package util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class HibernateUtil {

	private EntityManagerFactory emf;

	public EntityManager getEntityManager() {
		return getEntityManagerFactory().createEntityManager();
	}

	private EntityManagerFactory getEntityManagerFactory() {
		if (emf == null) {
			emf = Persistence.createEntityManagerFactory("tecnomotor");
		}
		return emf;
	}
}


