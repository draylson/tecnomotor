package entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity(name = "APLICACAO")
public class Aplicacao {

	@Id
	@GeneratedValue
	@Column(name = "APLID")
	private Long id;
	
	@Column(name = "APLANOINICIAL")
	private int anoInicial;
	
	@Column(name = "APLANOFINAL")
	private int anoFinal;
	
	@ManyToOne
	@JoinColumn(name = "TPSID")
	private TipoSistema tipoSistema;
	
	@ManyToOne
	@JoinColumn(name = "SISID")
	private Sistema sistema;
	
	@ManyToOne
	@JoinColumn(name = "MONID")
	private Montadora montadora;
	
	@ManyToOne
	@JoinColumn(name = "VEIID")
	private Veiculo veiculo;
	
	@ManyToMany(cascade = {CascadeType.MERGE})
	private List<Conector> conectores = new ArrayList<Conector>();

	public Long getId() {
		return id;
	}

	public int getAnoInicial() {
		return anoInicial;
	}

	public void setAnoInicial(int anoInicial) {
		this.anoInicial = anoInicial;
	}

	public int getAnoFinal() {
		return anoFinal;
	}

	public void setAnoFinal(int anoFinal) {
		this.anoFinal = anoFinal;
	}

	public TipoSistema getTipoSistema() {
		return tipoSistema;
	}

	public void setTipoSistema(TipoSistema tipoSistema) {
		this.tipoSistema = tipoSistema;
	}

	public Sistema getSistema() {
		return sistema;
	}

	public void setSistema(Sistema sistema) {
		this.sistema = sistema;
	}

	public Montadora getMontadora() {
		return montadora;
	}

	public void setMontadora(Montadora montadora) {
		this.montadora = montadora;
	}

	public Veiculo getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	
	public void addConector(Conector conector) {
		conectores.add(conector);
	}
	
	public void removeConector(Conector conector) {
		conectores.remove(conector);
	}
	
	public List<Conector> getConectores() {
		return this.conectores;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Aplicacao other = (Aplicacao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Aplicacao [id=" + id + ", anoInicial=" + anoInicial + ", anoFinal=" + anoFinal + ", tipoSistema="
				+ tipoSistema + ", sistema=" + sistema + ", montadora=" + montadora + ", veiculo=" + veiculo
				+ ", conectores=" + conectores + "]";
	}

}
