module tecnomotor {
	requires java.sql;
	requires java.persistence;
	requires org.hibernate.orm.core;
	requires org.hibernate.commons.annotations;
	requires javafx.graphics;
	requires javafx.fxml;
	requires javafx.controls;
	requires javafx.base;
	opens controller;
	opens dao;
	opens model;
	opens util;
	opens main;
	opens entity;
}