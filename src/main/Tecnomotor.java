package main;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;;

public class Tecnomotor extends Application {
	
	private static Stage primaryStage = null;
	
	public static Stage getPrimaryStage() {
		return primaryStage;
	}

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Tecnomotor.primaryStage = primaryStage;
		Parent root = FXMLLoader.load(this.getClass().getResource("/view/MainView.fxml"));
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Tecnomotor");
		primaryStage.setMaximized(true);
		primaryStage.show();
	}

}
