package dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import util.HibernateUtil;

public abstract class GenericDAO<T> {

	public void create(T entity) {

		EntityManager entityManager = new HibernateUtil().getEntityManager();
		entityManager.getTransaction().begin();
		entityManager.persist(entity);
		entityManager.getTransaction().commit();
		entityManager.close();

	}

	public void update(T entity) {

		EntityManager entityManager = new HibernateUtil().getEntityManager();
		entityManager.getTransaction().begin();
		entityManager.merge(entity);
		entityManager.getTransaction().commit();
		entityManager.close();

	}

	public void delete(T entity) {

		EntityManager entityManager = new HibernateUtil().getEntityManager();
		entityManager.getTransaction().begin();
		entityManager.remove(entityManager.contains(entity) ? entity : entityManager.merge(entity));
		entityManager.getTransaction().commit();
		entityManager.close();

	}

	public T read(Long id) {
		
		@SuppressWarnings("unchecked")
		Class<T> clazz = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		
		EntityManager entityManager = new HibernateUtil().getEntityManager();
		return (T) entityManager.find(clazz, id);
		
	}
	
	public List<T> readAll() {
		
		@SuppressWarnings("unchecked")
		Class<T> clazz = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		String name = clazz.getAnnotation(Entity.class).name();
		
		EntityManager entityManager = new HibernateUtil().getEntityManager();
		Query query = entityManager.createQuery("from " + name);
		
		@SuppressWarnings("unchecked")
		List<T> entities = (List<T>) query.getResultList();
		return entities;
		
	}

}
